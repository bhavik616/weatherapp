//
//  WeatherManager.swift
//  WeatherApp
//
//  Created by Bhavik on 20/05/17.
//  Copyright © 2017 Bhavik. All rights reserved.
//

import Foundation
import Alamofire

enum WeatherInfoResult {
    case success(dataModel : [DataModel])
    case error(NSError)
}

class WeatherManager {
    
    static let sharedInstance = WeatherManager()
    
    private let url = URL(string: "http://api.openweathermap.org/data/2.5/group?id=4163971,2147714,2174003&units=metric&appid=60f6db89f4e62fccbc96f572d3eaf24c")
    
    typealias jsonDictionary = Dictionary<String, AnyObject>
    
    private init() {}
    
    

    func downloadData(completed: @escaping ([DataModel]) -> ()) {
        
        var weatherInfo = [DataModel]()
        
        
        Alamofire.request(url!).responseJSON(completionHandler: {
            response in
           
                        
            let result = response.result
            
            let mainDict = result.value as? jsonDictionary
            
            let listArray = mainDict?["list"] as? [jsonDictionary]
            
            weatherInfo = self.getWeatherForAllCities(json: listArray ?? [])

            completed(weatherInfo)
        })
    }

    
    func getWeatherForAllCities(json : [jsonDictionary]?) -> [DataModel]{
        if let json = json {
            var weatherInfo = [DataModel]()
            
            for jsonObj in json{
                let currentWeatherInfo : DataModel = DataModel.init(json: jsonObj)
                weatherInfo.append(currentWeatherInfo)
            }
            return weatherInfo
        }
        else { return [] }
    }

}
