//
//  HomePageTableViewController.swift
//  WeatherApp
//
//  Created by Bhavik on 20/05/17.
//  Copyright © 2017 Bhavik. All rights reserved.
//

import UIKit
import CoreData

class HomePageTableViewController: UITableViewController {
    
    var weatherInfo : [DataModel]? = [DataModel]()

    var model : [Model] = []
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "WeatherApp"
        
         WeatherManager.sharedInstance.downloadData { result in
            self.weatherInfo = result
            self.tableView.reloadData()
            let _ = self.tableView(self.tableView, viewForHeaderInSection: 1)
            self.saveToCoreData(array : self.weatherInfo!)
        }
        
        self.getData()
        
        setupTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (weatherInfo?.isEmpty)!{
            if model.isEmpty{
                return 3
            } else {
                return model.count
            }
        } else {
        return weatherInfo!.count
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath) as? CityWeatherInfoCell
        
        if cell == nil {
            cell = CityWeatherInfoCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "weatherCell")
        }
        
        
        if !(weatherInfo?.isEmpty)! {
        // Naming the labels
        
        cell?.cityLabel.text = weatherInfo?[indexPath.row].location ?? "Sydney"
        
        cell?.weatherLabel.text = weatherInfo?[indexPath.row].weather ?? "Cloudy"
        
        cell?.weatherDescription.text = weatherInfo?[indexPath.row].weatherDescription ?? "Scattered Clouds"
        
        cell?.temperatureLabel.text = weatherInfo?[indexPath.row].temp ?? "30°C"
        
        cell?.weatherIcon.image = UIImage.init(named: weatherInfo?[indexPath.row].weatherDescription ?? "Sunny")
         
            return cell!
        
        } else if !model.isEmpty {
            
            cell?.cityLabel.text = model[indexPath.row].cityName
            
            cell?.weatherLabel.text = model[indexPath.row].weather
            
            cell?.weatherDescription.text = model[indexPath.row].weatherDescription
            
            cell?.temperatureLabel.text = model[indexPath.row].temperature
            
            cell?.weatherIcon.image = UIImage.init(named: model[indexPath.row].weatherDescription!)
            
            return cell!
        } else {
        return cell!
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40))
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y:5, width: self.view.frame.width, height: 25))
        
        if (weatherInfo?.isEmpty)! {
        titleLabel.text = "Weather Today"
        } else {
        titleLabel.text = weatherInfo?[section].date
        }
        
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont(name: "Arial", size: 22)
        headerView.addSubview(titleLabel)

        return headerView
}
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }


    // MARK: - Setting up UI
    
    func setupTableView() {
        self.tableView.register(UINib.init(nibName: "CityWeatherInfoCell", bundle: nil), forCellReuseIdentifier: "weatherCell")
        self.tableView.estimatedRowHeight = 44
        
    }
    
    // MARK: - Core Data
    
    func saveToCoreData(array : [DataModel]){
        
        
        for obj in array {
            let model = Model(context: context)
            model.cityName = obj.location
            model.date = obj.date
            model.temperature = obj.temp
            model.weather = obj.weather
            model.weatherDescription = obj.weatherDescription
            model.weatherIconName = obj.weatherDescription
        }
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    
    func getData() {
        do {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Model")
            fetchRequest.returnsObjectsAsFaults = false
            model = try context.fetch(fetchRequest) as! [Model]
        } catch {
            print("Fetching Failed")
        }
    }

    
    
    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
