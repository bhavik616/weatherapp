//
//  DataModel.swift
//  WeatherApp
//
//  Created by Bhavik on 21/05/17.
//  Copyright © 2017 Bhavik. All rights reserved.
//

import Foundation
import Alamofire

class DataModel {
    
    var date: String?
    var temp: String?
    var location: String?
    var weather: String?
    var weatherDescription : String?
    var country : String?
    typealias jsonDictionary = Dictionary<String, AnyObject>
    
    
    init(json : jsonDictionary?) {

            let dict = json
            let main = dict?["main"] as? jsonDictionary
            let temp = main?["temp"] as? Double
        if let temp = temp {
            self.temp = String("\(temp)°C")
            }else {
            self.temp = "0 °C"
        }
        
            let name = dict?["name"] as? String
            self.location = name ?? "Location Invalid"
        
            let weatherArray = dict?["weather"] as? [jsonDictionary]
            let weather = weatherArray?[0]["main"] as? String
            self.weather = weather ?? "Weather Invalid"
        
            let weatherDescription = weatherArray?[0]["description"] as? String
            self.weatherDescription = weatherDescription ?? "Weather Description Unavailable"
        
        
            let sys = dict?["sys"] as? jsonDictionary
            let country = sys?["country"] as? String
            self.country = country ?? "Country Invalid"
        
            let dt = dict?["dt"] as? Double
            self.date = self.dateFormatter(date: dt)
}
    
    
    func dateFormatter(date : Double?) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        
        if let dt = date{
        let date = Date(timeIntervalSince1970: dt)
        return "Today, \(dateFormatter.string(from: date))"
        } else {
        return "Date Invalid"
        }
    }
}
